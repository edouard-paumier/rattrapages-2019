# Transposition du rattrapages de l'année 2019 en version Moodle

1. Forkez le projet
    * URL du projet sur GitLab : https://gitlab.com/edouard-paumier/rattrapages-2019
2. Clonez-le localement (avec GitKraken ou SourceTree par exemple)
3. Créez une Merge Request de votre master vers votre prod
4. Ouvrez le projet dans IntelliJ
5. Rafraîchissez la configuration du projet à partir du pom.xml
6. Suivez le [sujet d'examen](https://www.myefrei.fr/moodle/mod/quiz/view.php?id=34902)
7. Comitez vos changements
8. Soumettez une Merge Request de **votre** master vers **votre** prod
9. Vérifiez que votre Merge Request comprend bien vos changements (onglet changes)
10. Soumettez le Quizz
