package fr.efrei.rattrapages.rattrapage2019;

public class Amelioration1 {
    public interface SoundSystem {
        void setSource(String sourceId);
        void setVolume(int volume);
        void startMusic(String playlist);
        void stopMusic();
    }

    public class Alarm implements Runnable {
        private final SoundSystem s1;
        private final String s2;
        private final String s3;
        private final int j;
        private final int k;

        private final Object o1 = new Object();
        private Thread o2;

        public Alarm(SoundSystem s1, String s2, String s3, int j, int k) {
            this.s1 = s1;
            this.s2 = s2;
            this.s3 = s3;
            this.j = j;
            this.k = k;
        }

        public void start() {
            synchronized (o1) {
                if (o2 == null) {
                    o2 = new Thread(this);
                    o2.start();
                }
            }
        }

        public void stop() throws InterruptedException {
            synchronized (o1) {
                if (o2 != null) {
                    o2.interrupt();
                    o2.join();
                    o2 = null;
                }
            }
        }

        @Override
        public void run() {
            s1.setSource(s2);
            s1.setVolume(0);
            s1.startMusic(s3);

            try {
                int i = 0;
                while (true) {
                    Thread.sleep(j);

                    if (i < k)
                        i++;
                    s1.setVolume(i);
                }
            } catch (InterruptedException e) {
            }

            s1.stopMusic();
        }
    }
}
